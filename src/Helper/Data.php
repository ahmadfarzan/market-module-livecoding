<?php

namespace Market\LiveCoding\Helper;

use Market\LiveCoding\FileProcessor;
use Market\LiveCoding\Parser\ParserInterface;
use Market\LiveCoding\Reader\ReaderInterface;

class Data
{

    /** @var FileProcessor */
    protected $fileProcessor;

    /**
     * Data constructor.
     * @param FileProcessor $fileProcessor
     */
    public function __construct(
        FileProcessor $fileProcessor
    )
    {
        $this->fileProcessor = $fileProcessor;
    }

    /**
     * @param $source
     * @return \Market\LiveCoding\Model\OfferCollectionInterface
     * @throws \Exception
     */
    public function parse($source): \Market\LiveCoding\Model\OfferCollectionInterface
    {
        $processer = $this->fileProcessor->loadBySource($source);
        /** @var ReaderInterface $reader */
        $reader = $processer->getReader();
        /** @var ParserInterface $parser */
        $parser = $processer->getParser();

        $data = $reader->read($source);

        $parsedData = $parser->parse($data);

        return $parsedData;
    }

    /**
     * @param \Market\LiveCoding\Model\OfferCollectionInterface $parsedData
     * @param $field
     * @param $arg1
     * @param $arg2
     * @return \Market\LiveCoding\Model\OfferCollectionInterface
     * @throws \Exception
     */
    public function filter($parsedData, $field, $arg1, $arg2): \Market\LiveCoding\Model\OfferCollectionInterface
    {
        if (!$parsedData->count()) {
            throw new \Exception('Data not found in source.');
        }
        $filteredData = $parsedData->filter($field, $arg1, $arg2);
        return $filteredData;
    }

}