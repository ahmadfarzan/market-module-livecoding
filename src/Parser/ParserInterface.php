<?php

namespace Market\LiveCoding\Parser;

use Market\LiveCoding\Model\OfferCollectionInterface;

interface ParserInterface {

    public function parse(string $input): OfferCollectionInterface;
}