<?php

namespace Market\LiveCoding\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Market\LiveCoding\Helper\Data as Helper;

class FilterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'market:filter {field} {--source=} {--arg1=} {--arg2=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Filter market data';

    protected $fileProcessor;

    protected $helper;

    /**
     * Create a new command instance.
     *
     * FilterCommand constructor.
     * @param Helper $helper
     */
    public function __construct(
        Helper $helper
    )
    {
        parent::__construct();

        $this->helper = $helper;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws \Exception
     */
    public function handle()
    {
        $field = $this->argument('field');
        $arg1 = $this->option('arg1');
        $arg2 = $this->option('arg2');
        $source = $this->option('source');

        if (empty($arg1)) {
            return $this->output->error('arg1 can not be empty');
        }

        if (empty($source)) {
            return $this->output->error('Source can not be empty');
        }

        try {
            /** @var \Market\LiveCoding\Model\OfferCollectionInterface $parsedData */
            $parsedData = $this->helper->parse($source);
            $filteredData = $this->helper->filter($parsedData, $field, $arg1, $arg2);

            $this->table(['id', 'price', 'quantity'], $filteredData->__toArray());
        } catch (\Exception $e) {
            $this->output->error($e->getMessage());
            Log::error($e->getMessage());
        }
    }
}
