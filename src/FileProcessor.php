<?php

namespace Market\LiveCoding;

use Market\LiveCoding\FileProcessor\AbstractFactory;
use Market\LiveCoding\FileProcessor\CSVFactory;
use Market\LiveCoding\FileProcessor\JSONFactory;
use Market\LiveCoding\FileProcessor\XMLFactory;

class FileProcessor
{
    protected $reader;
    protected $parser;

    public function getReader()
    {
        return $this->reader;
    }

    public function getParser()
    {
        return $this->parser;
    }

    /**
     * @param $source
     * @return $this
     * @throws \Exception
     */
    public function loadBySource($source)
    {
        $parserFactories = [
            'csv' => CSVFactory::class,
            'json' => JSONFactory::class,
            'xml' => XMLFactory::class,
        ];
        $parserFactoryFlag = false;

        /** @var AbstractFactory $parserFactory */
        foreach ($parserFactories as $ext => $parserFactory) {
            if ($parserFactory::isValidFile($source)) {
                $this->create(new $parserFactory);
                $parserFactoryFlag = true;
            }
        }
        if (!$parserFactoryFlag) {
            throw new \Exception('Unsupported file format.');

        }
        return $this;
    }

    /**
     * @param AbstractFactory $abstractProcessorFactory
     * @return void
     */
    protected function create(AbstractFactory $abstractProcessorFactory)
    {
        $this->reader = $abstractProcessorFactory->createReader();
        $this->parser = $abstractProcessorFactory->createParser();
    }

}