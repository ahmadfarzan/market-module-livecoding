<?php

namespace Market\LiveCoding\FileProcessor;

use Market\LiveCoding\Parser\CSVParser;
use Market\LiveCoding\Parser\ParserInterface;
use Market\LiveCoding\Reader\CSVReader;
use Market\LiveCoding\Reader\ReaderInterface;

class CSVFactory extends AbstractFactory
{

    public function createParser(): ParserInterface
    {
        return new CSVParser();
    }

    public function createReader(): ReaderInterface
    {
        return new CSVReader();
    }

    public static function isValidFile($source): bool
    {
        $handle = fopen($source, 'r');

        if ($handle) {
            while (($line = fgetcsv($handle)) !== false) {
                fclose($handle);
                if (count($line) <= 1) {
                    return false;
                }
                return true;
            }
            fclose($handle);
            return false;
        }
    }
}