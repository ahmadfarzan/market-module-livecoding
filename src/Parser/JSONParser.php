<?php

namespace Market\LiveCoding\Parser;

use Market\LiveCoding\Model\Offer;
use Market\LiveCoding\Model\OfferCollectionInterface;

class JSONParser extends AbstractParser implements ParserInterface
{
    /**
     * @param string $input
     * @return OfferCollectionInterface
     * @throws \Exception
     */
    public function parse(string $input): OfferCollectionInterface
    {
        $data = json_decode($input, true);
        if (empty($data)) {
            throw new \Exception('Error while parsing this JSON file.');
        }

        /** @var array $record */
        foreach ($data['special_offers'] as $record) {
            /** @var Offer $offer */
            $offer = $this->offerFactory->create();
            $offer->setId(isset($record['id']) ? $record['id'] : null);
            $offer->setPrice(isset($record['price']) ? $record['price'] : 0);
            $offer->setQuantity(isset($record['quantity']) ? $record['quantity'] : 0);

            $this->offerCollection->add($offer);
        }

        return $this->offerCollection;
    }

}