<?php

namespace Market\LiveCoding\Reader;

use Market\LiveCoding\Model\OfferCollectionInterface;

interface ReaderInterface {

    public function read(string $source): string;
}