<?php

namespace Market\LiveCoding;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

use Market\LiveCoding\Console\FilterCommand;

class ServiceProvider extends BaseServiceProvider
{

    /**
     * Boot the service provider.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                FilterCommand::class,
            ]);
        }

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}
