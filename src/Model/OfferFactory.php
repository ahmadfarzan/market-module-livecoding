<?php

namespace Market\LiveCoding\Model;


class OfferFactory implements OfferFactoryInterface
{
    public function create(): Offer
    {
        return new Offer();
    }
}