<?php

namespace Market\LiveCoding\Reader;

abstract class AbstractReader implements ReaderInterface
{
    /**
     * @inheritDoc
     */
    public function read(string $input): string
    {
        return file_get_contents($input);
    }
}