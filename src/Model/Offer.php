<?php

namespace Market\LiveCoding\Model;


class Offer extends DTO implements OfferInterface
{
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    public function setId($id)
    {
        $this->_setData(self::ID, $id);
    }

    public function getPrice()
    {
        return $this->_getData(self::PRICE);
    }

    public function setPrice($price)
    {
        $this->_setData(self::PRICE, $price);
    }

    public function getQuantity()
    {
        return $this->_getData(self::QUANTITY);
    }

    public function setQuantity($quantity)
    {
        $this->_setData(self::QUANTITY, $quantity);
    }
}