<?php

namespace Market\LiveCoding\Model;


class OfferCollection implements OfferCollectionInterface
{
    protected $offers = [];

    /**
     * @inheritDoc
     */
    public function add(OfferInterface $offer): void
    {
        $this->offers[] = $offer;
    }

    /**
     * @inheritDoc
     */
    public function get(int $index): OfferInterface
    {
        if (!isset($this->offers[$index])) {
            throw new \Exception('Offer not found.');
        }
        return $this->offers[$index];
    }

    public function count(): int
    {
        return count($this->getAll());
    }

    /**
     * @inheritDoc
     */
    public function getIterator(): Iterator
    {
        // TODO: Implement getIterator() method.
    }

    /**
     * @inheritDoc
     */
    public function getAll(): array
    {
        return $this->offers;
    }

    public function filter($field, $arg1, $arg2 = false): OfferCollection
    {
        /** @var OfferCollection $offerCollection */
        $offerCollection = new $this();
        /** @var OfferInterface $offer */
        foreach ($this->getAll() as $offer) {
            if (empty($arg2) && $offer->getData($field) == $arg1) {
                $offerCollection->add($offer);
            } else if (!empty($arg2) && $offer->getData($field) >= $arg1 && $offer->getData($field) <= $arg2) {
                $offerCollection->add($offer);
            }
        }
        return $offerCollection;
    }

    public function __toArray(): array
    {
        $offers = $this->getAll();
        $array = [];
        foreach ($offers as $offer) {
            $array[] = $offer->__toArray();
        }

        return $array;
    }

}