<?php

namespace Market\LiveCoding\Parser;

use Market\LiveCoding\Model\OfferCollectionInterface;

class XMLParser extends AbstractParser implements ParserInterface
{
    /**
     * @param string $input
     * @return OfferCollectionInterface
     */
    public function parse(string $input): OfferCollectionInterface
    {
        $data = simplexml_load_string($input);

        /** @var \SimpleXMLElement $element */
        foreach ($data->special_offers->element as $element) {
            $offer = $this->offerFactory->create();
            $offer->setId(isset($element->id) ? (string)$element->id : null);
            $offer->setPrice(isset($element->price) ? (string)$element->price : 0);
            $offer->setQuantity(isset($element->quantity) ? (string)$element->quantity : 0);

            $this->offerCollection->add($offer);
        }

        return $this->offerCollection;
    }
}