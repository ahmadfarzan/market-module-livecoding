<?php

namespace Market\LiveCoding\Model;

/**
 * Interface for The Collection class that contains Offers
 */
interface OfferCollectionInterface
{
    /**
     * Add offer to the collection
     *
     * @param OfferInterface $offer
     * @return void
     */
    public function add(OfferInterface $offer): void;

    /**
     * Get offer at specific index
     *
     * @param int $index
     * @return OfferInterface
     * 2
     */
    public function get(int $index): OfferInterface;

    public function getAll(): array;

    public function filter($field, $arg1, $arg2): OfferCollection;

    public function count(): int;

    /**
     * @return Iterator
     */
    public function getIterator(): Iterator;

    public function __toArray(): array;
}