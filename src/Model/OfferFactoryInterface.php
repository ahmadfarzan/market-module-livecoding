<?php

namespace Market\LiveCoding\Model;

interface OfferFactoryInterface
{
    public function create(): Offer;
}