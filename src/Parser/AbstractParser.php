<?php

namespace Market\LiveCoding\Parser;

use Market\LiveCoding\Model\OfferCollection;
use Market\LiveCoding\Model\OfferFactory;
use Market\LiveCoding\Model\OfferFactoryInterface;

abstract class AbstractParser
{
    /** @var OfferFactoryInterface $offerFactory */
    protected $offerFactory;

    /** @var OfferCollection $offerCollection */
    protected $offerCollection;

    /**
     * AbstractParser constructor.
     */
    public function __construct()
    {
        $this->offerFactory = new OfferFactory();
        $this->offerCollection = new OfferCollection();
    }
}